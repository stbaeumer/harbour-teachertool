# README #

## Summary

Teachertool calculates the grade out of reached points.

## Description

Let's say you are a teacher, sitting at home and correcting a test.  As usual you have determined the amount of points and measured the achieved score for every student.

Now teachertool comes into play and calculates the grade for every student, consistent with your assessment scale.


## Pix

### Startpage

![FirstPage.png](https://bitbucket.org/repo/9k77Kg/images/690730066-FirstPage.png)



### Diagram

![Diagram.png](https://bitbucket.org/repo/9k77Kg/images/371847980-Diagram.png)



### Shuffle

![ShuffleNumbers.png](https://bitbucket.org/repo/9k77Kg/images/4077599823-ShuffleNumbers.png)

* Generate random number
* Define your own range

![ShuffleMenu.png](https://bitbucket.org/repo/9k77Kg/images/2447674853-ShuffleMenu.png)

* Generate random letters
* Generate random grade of your scale
* Define your own range

### Edit, delete, create 

![EditScale.png](https://bitbucket.org/repo/9k77Kg/images/857566438-EditScale.png) 

* Edit, delete, create existing or own scales

![Details.png](https://bitbucket.org/repo/9k77Kg/images/727142882-Details.png)

* Edit, delete, create existing or own grades

### Comparison

![Comparison.png](https://bitbucket.org/repo/9k77Kg/images/3475366619-Comparison.png)

* Compare two scales of your choice

![Comparison2.png](https://bitbucket.org/repo/9k77Kg/images/3282746262-Comparison2.png)

* Show details of two scales