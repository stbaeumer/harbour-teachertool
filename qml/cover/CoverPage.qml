/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    id: coverBackground

    function renderCover()
    {
        if(number === 0)
        {
            pic.source = "harbour-teachertool.png";
            return qsTr("Teachertool");
        }
        else
        {
            pic.source = avgIndex + ".png";
            return "Teachertool\n" + " ∑ " + number + "\n Ø " + avgPercent + " %\n Ø " + avgGrade;
        }
    }

    function renderPicSource ()
    {
        if(number === 0)
        {
            pic.source = "harbour-teachertool.png";
        }
        else
        {
            pic.source = avgIndex + ".png";
        }
    }

    Rectangle{
    id:rectCover
    width: parent.width
    height: parent.height
    anchors.fill: parent
    color: "transparent"
        Rectangle{
        id: rect1
        color: "transparent"
        width: rectCover.width
        height: rectCover.height / 2
        anchors.top: rectCover.TopLeft
            Label {
                id: label
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                text: renderCover()
            }
        }
        Rectangle{
        id: rect2
        color: "transparent"
        width: rectCover.width
        height: rectCover.height / 2 - 35
        anchors.top: rect1.bottom
            Image {
                id: pic
                source: renderPicSource()
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                height: rect2.height - 20
                width: height * 0.77
            }
        }
        Rectangle{
        id: rect3
        color: "green"
        width: rectCover.width
        height: 35
        anchors.top: rect2.bottom
            Text {
                id: picText
                text: " Teachertool"
                color: "white"
                font.pointSize: 20
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                height: 35
                width: rect3.width
            }
        }
    }
}



