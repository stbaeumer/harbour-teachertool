import QtQuick 2.0;
import Sailfish.Silica 1.0;
import "../config.js" as DB;

Page {
    id: page;
    allowedOrientations: Orientation.Portrait;

    Component.onCompleted: {
        DB.initialize();        
        DB.getModel()
        DB.getAssessmentGradeIndex();        
        editLmDetailsModel(assessmentScaleIndex);
    }

    function editLmDetailsModel(i){
        lmDetails.clear();

        if(lmm.count > 0){
            var y = lmm.get(i).steps.split(",");
            var z = lmm.get(i).grades.split(",");
            var fromPercent = 0;

            for (var j = 0; j < z.length; j++){
                if(j > 0){
                    fromPercent = parseInt(lmDetails.get(j - 1).upToLessThanPercent);
                }
                lmDetails.append({grade: z[j], fromPercent: fromPercent, upToLessThanPercent: y[j], frequency: 0})
            }
        }
        return lmDetails;
    }

    onVisibleChanged: {
        if (visible) {
            if(lmm.count > 0){
                if(btnGradeSelector.text !== lmm.get(assessmentScaleIndex).name){
                    btnGradeSelector.text = lmm.get(assessmentScaleIndex).name;
                    number = 0;
                    tbxMaxPoints.text = "";
                    tbxMaxPoints.label = "Out of a possible 100 points ...";
                    tbxReachedPoints.label = missedOrReachedLabel();
                    tbxReachedPoints.text = "";
                    reachedPoints = 0;
                    maxPoints = 100;                    
                    reset();
                }
            }else{
                btnGradeSelector.text = "Click to create scale!";
                lmDetails.clear();
            }
        }
    }

    function getMin() {
        if(mode===0) { 
            var min = (lmDetails.get(gradeIndex).fromPercent / 100 * maxPoints).toFixed();
            reachedPoints = min;
            return min;
        }
        if(mode===1) {
            var min = (maxPoints - (lmDetails.get(gradeIndex).fromPercent / 100 * maxPoints)).toFixed();
            reachedPoints = (maxPoints - min);
            return min;
        }
    }

    function getMax() {        
        if(mode===0) {
            var max = (Math.min(lmDetails.get(gradeIndex).upToLessThanPercent,100)  / 100 * maxPoints);
            if(max === maxPoints) {
                return max.toFixed();
            }
            else{
                return "below " + max.toFixed();
            }
        }
        if(mode===1) {
            max = (maxPoints - (Math.min(lmDetails.get(gradeIndex).upToLessThanPercent,100)  / 100 * maxPoints)).toFixed();
        }
    }

    function getIndexByPercent() {
        if (lmDetails.count > 0) {            
            var achievedPercent =  100 * (reachedPoints / maxPoints);
            for (var i = 0; i < lmDetails.count; i++) {
                if (parseInt(lmDetails.get(i).fromPercent) <= achievedPercent && parseInt(lmDetails.get(i).upToLessThanPercent) > achievedPercent) {
                    return i;
                }
                if(achievedPercent == 100){
                    return lmDetails.count - 1;
                }
            }
            return -1;
        }
    }

    function colorizeButtons() {
        for (var i = 0; i < lmDetails.count ; ++i) {
            if (gradeIndex === i) {                
                btnRepeater.itemAt(i).color = "yellow";                
            }else{                
                btnRepeater.itemAt(i).color = "white";
            }
        }
    }

    function getCoordinateX(index) {    

        if(index === 0 || index === 4 || index === 8 || index === 12 || index === 16) {
            return page.width * 0.1;
        }
        if(index === 1 || index === 5 || index === 9 || index === 13)  {
            return page.width * 0.3;
        }
        if(index === 2 || index === 6 || index === 10 || index === 14) {
            return page.width * 0.5;
        }
        if(index === 3 || index === 7 || index === 11 || index === 15) {
            return page.width * 0.7;
        }
        return false
    }
    
    function getCoordinateY(index) {        
        var y = 0;
        if(index > 0 && index <=3) {
            return 0;
        }
        if(index > 3 && index <=7) {
            return 0 + page.height * 0.1;
        }
        if(index > 7 && index <= 11) {
            return 0 + page.height * 0.2;
        }
        if(index > 11 && index <= 15) {
            return 0 + page.height * 0.3;
        }
        if(index > 15 && index <= 20) {
            return 0 + page.height * 0.4;
        }
        return y;
    }

    function isInt(n) {
        return n !== "" && n % 1 === 0;
    }

    function reset()
    {
        console.log("Reset");
        sumOfAllIndexes = 0;
        sumOfAllPercents = 0;
        avgGrade = 0;
        avgPercent = 0;
        avgIndex = 0;
        number = 0;
        for (var i = 0; i < lmDetails.count ; ++i) {
            lmDetails.get(i).frequency = 0;
        }

        tbxMaxPoints.text = "";
        tbxMaxPoints.label = "Out of a possible 100 points ...";
        tbxReachedPoints.label = missedOrReachedLabel();
        tbxReachedPoints.text = "";
        reachedPoints = 0;
        maxPoints = 100;

        btnEvaluation.text = "∑ " + number + "  Ø " + avgPercent + "%  Ø " + avgGrade;
    }

    function iterateFrequency()
    {
        if(maxPoints >= reachedPoints) {
            var i = getIndexByPercent();
            lmDetails.get(i).frequency = lmDetails.get(i).frequency + 1;
            number++;            
            sumOfAllIndexes = sumOfAllIndexes + gradeIndex;
            sumOfAllPercents = sumOfAllPercents + 100 * (reachedPoints / maxPoints);
            avgPercent = Math.round((sumOfAllPercents / number) * 100) / 100;
            avgIndex = (Math.round(sumOfAllIndexes / number) * 100) / 100;
            avgGrade =  lmDetails.get(avgIndex).grade;
            btnEvaluation.text = "∑ " + number + "  Ø " + avgPercent + "%  Ø " + avgGrade;     
        }
    }

    function missedOrReachedLabel() {
        if(mode === 0) {
            return "... 0 points have been reached";
        }
        if(mode === 1) {
            return "... all points have been missed";
        }
    }

    function missedOrReachedLabelByReachedPoints(reachedP) {
        if(mode === 0) {
            return "... " + reachedP + " points have been reached.";
        }
        if(mode === 1) {
            return "... " + (maxPoints - reachedP) + " points have been missed.";
        }
    }

    function missedOrReachedTextByReachedPoints(reachedP) {
        if(mode === 0) {
            return reachedP;
        }
        if(mode === 1) {
            return (maxPoints - reachedP);
        }
    }

    function setReachedPoints(text) {
        if(mode === 0) {
            reachedPoints = text;
        }
        if(mode === 1) {
            reachedPoints = maxPoints - text;
        }
    }

    function tbxReachedPointsPlaceholder() {
        if(mode === 0) {
            return "... " + 0 + " points have been reached.";
        }
        if(mode === 1) {
            return "... " + maxPoints + " points have been missed.";
        }
    }
    
    SilicaFlickable {
        anchors.fill: parent;
        PullDownMenu {            
            MenuItem {
                text: qsTr("Diagram");
                onClicked: pageStack.push(Qt.resolvedUrl("AssessmentGrades.qml"));
            }
            /*MenuItem {
                text: qsTr("Scale in detail");
                onClicked: pageStack.push(Qt.resolvedUrl("ScaleDetails.qml"));
            }*/
            MenuItem {
                text: qsTr("Scale comparison");
                onClicked: pageStack.push(Qt.resolvedUrl("ScaleComparisonSelect.qml"));
            }            
            MenuItem {
                text: qsTr("Shuffle");
                onClicked: pageStack.push(Qt.resolvedUrl("Shuffle.qml"));
            }
            MenuItem {
                text: qsTr("About this app");
                onClicked: pageStack.push(Qt.resolvedUrl("AboutApp.qml"));
            }            
        }

        PushUpMenu {
            MenuItem {
                text: qsTr("Reset")
                onClicked: {
                    reset();
                }
            }
            /*
            MenuItem {
                text: qsTr("Define class")
                onClicked: {

                }
            }
            MenuItem {
                text: qsTr("Define examination")
                onClicked: {
                    reset();
                }
            }*/
            /*
            MenuItem {
                text: qsTr("Reached Points")
                onClicked: {
                    mode = 0;
                    tbxReachedPoints.text = missedOrReachedTextByReachedPoints(reachedPoints);
                    tbxReachedPoints.label = missedOrReachedLabelByReachedPoints(reachedPoints);
                }
            }
            MenuItem {
                text: qsTr("Missed Points")
                onClicked: {
                    mode = 1;
                    tbxReachedPoints.text = missedOrReachedTextByReachedPoints(reachedPoints);
                    tbxReachedPoints.label = missedOrReachedLabelByReachedPoints(reachedPoints);
                }
            }*/
        }

        Column {
            id: column;
            width: page.width;
            spacing: Theme.paddingLarge;
            PageHeader {
                title: "Teachertool";
            }

            Button{
                id: btnGradeSelector;
                width: page.width - 80;
                x: 40;
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("ScalesListview.qml"));
                }
            }
            /*
            Row{
                spacing: 12
                x: 6

                Button{
                    id: btnGradeSelector;
                    width: page.width / 3 - 12;

                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("ScalesListview.qml"));
                    }
                }
                Button{
                    id: btnClass;
                    //x: (page.width - 400) / 2;

                    width: page.width / 3 - 12;
                    text: "HBFGO1"
                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("ScalesListview.qml"));
                    }
                }
                Button{
                    id: btnExamination;

                    //x: (page.width - 400) / 2;
                    width: page.width / 3 - 12;
                    text: "K3"
                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("ScalesListview.qml"));
                    }
                }
            }
            Row{
                spacing: 12
                x: 6
                Button{
                    id: btnBack;
                    width: 80;
                    text: "<"
                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("ScalesListview.qml"));
                    }
                }
                Button{
                    id: btnStudent;
                    width: page.width - 160 - 24 - 12;
                    text: "Tom Saywer"
                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("ScalesListview.qml"));
                    }
                }
                Button{
                    id: btnNext;
                    width: 80;
                    text: ">"
                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("ScalesListview.qml"));
                    }
                }
            }*/

            TextField {
                        id: tbxMaxPoints;
                        width: parent.width;
                        label: "Out of a possible 100 points ...";
                        placeholderText: "Out of a possible 100 points ...";
                        focusOnClick: true;
                        inputMethodHints: Qt.ImhFormattedNumbersOnly;
                        onClicked: { focus = true; text = "" }
                        EnterKey.onClicked:{ parent.focus = true }
                        onTextChanged: {
                            if(focus) {                                
                                //reset();
                                if(isInt(tbxMaxPoints.text)) {
                                    maxPoints = tbxMaxPoints.text;
                                }
                                else {
                                    maxPoints = 100;
                                }
                                if(reachedPoints > maxPoints) {
                                    tbxMaxPoints.label = "The max. reachable points must be greater ...";
                                    tbxReachedPoints.label = "... than the actual output.";
                                    gradeIndex = -1;
                                }
                                else {
                                    tbxMaxPoints.label = "Out of a possible " + maxPoints + " points ...";
                                    tbxReachedPoints.label = missedOrReachedLabelByReachedPoints(reachedPoints);
                                    gradeIndex = page.getIndexByPercent();
                                }
                                colorizeButtons();
                            }
                        }
                    }

            TextField {
                id: tbxReachedPoints;
                width: parent.width;
                label: "... 0 points have been reached.";
                inputMethodHints: Qt.ImhFormattedNumbersOnly;
                focusOnClick: true;
                placeholderText: tbxReachedPointsPlaceholder();
                onClicked: { focus = true; text = ""}
                EnterKey.onClicked:{ parent.focus = true; iterateFrequency()}
                onTextChanged: {
                    if(focus) {                        
                        if(isInt(tbxReachedPoints.text)) {                            
                            setReachedPoints(tbxReachedPoints.text);
                        }
                        else {
                            reachedPoints = 0;
                        }
                        if(reachedPoints > maxPoints) {
                            tbxMaxPoints.label = "The max. reachable points must be greater ...";
                            tbxReachedPoints.label = "... than the actual output.";
                            gradeIndex = -1;
                        }
                        else {
                            tbxMaxPoints.label = "Out of a possible " + maxPoints + " points ...";
                            tbxReachedPoints.label = missedOrReachedLabelByReachedPoints(reachedPoints);
                            gradeIndex = page.getIndexByPercent();                            
                        }
                        colorizeButtons();
                    }
                }
            }

            Item{
                id: rect;
                width: parent.width;
                height: 400;
                Repeater {
                    id: btnRepeater;
                    model: lmDetails;
                    Button {anchors.top: rect.TopLeft; x:getCoordinateX(index); y:getCoordinateY(index); preferredWidth: page.width * 0.17; text: grade;
                        onClicked: {
                            gradeIndex = index;
                            tbxMaxPoints.text = maxPoints;
                            tbxMaxPoints.label = "To get the grade \'" + grade + "\' out of " + maxPoints + " points ...";
                            tbxReachedPoints.text = getMin()//models[assessmentScaleIndex].get(gradeIndex).fromPercent;
                            tbxReachedPoints.label = "... " + getMin() + " up to " + getMax() + " points have to be reached.";
                            colorizeButtons();                                                                                   
                            frequency = frequency + 1;
                            number++;
                            sumOfAllIndexes = sumOfAllIndexes + gradeIndex;
                            sumOfAllPercents = sumOfAllPercents + 100 * (reachedPoints / maxPoints);
                            avgPercent = Math.round((sumOfAllPercents / number) * 100) / 100;
                            avgIndex = (Math.round(sumOfAllIndexes / number) * 100) / 100;
                            avgGrade =  lmDetails.get(avgIndex).grade;
                            btnEvaluation.text = "∑ " + number + "  Ø " + avgPercent + "%  Ø " + avgGrade;
                        }
                    }
                }
                Button {
                    id:btnEvaluation; 
                    opacity: 1; 
                    anchors.top: 
                    rect.TopLeft; 
                    x:40;
                    y:getCoordinateY(lmDetails.count + 3);
                    width: page.width - 80;
                    color: "orange"; 
                    text: "Teachertool"
                    onClicked: pageStack.push(Qt.resolvedUrl("AssessmentGrades.qml"))}
            }
        }
    }
}
