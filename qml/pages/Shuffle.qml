import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page
    allowedOrientations: Orientation.Portrait

    Component {
    id: menuItemComp
    MenuItem {}
    }

    Component.onCompleted: {
        shuffleModel.clear();

        shuffleModel.append({ text: "number", tbxMin: "1", lblMin: "Fill in minimum number (1, 2, ...)"       , tbxMax: "15", lblMax: "Fill in maximum number (..., 14, 15)"    , btnText: "Generate number"});
        shuffleModel.append({ text: "letter", tbxMin: "A", lblMin: "Fill in first letter of range (A, B, ...)", tbxMax: "Z" , lblMax: "Fill in last letter of range (..., Y, Z)", btnText: "Generate letter"});

        if(lmm.count > 0){
                   shuffleModel.append({text: lmm.get(assessmentScaleIndex).name , tbxMin: "" , lblMin: "Fill in lowest grade of range"            , tbxMax: ""  , lblMax: "Fill in highest grade of range"          , btnText: "Generate grade"});
            lblError.text = "";
        }else{
            lblError.text = "You need to create at least one scale to generate random grades.";
        }
        for (var i = 0; i < shuffleModel.count ; i++){

            var newMenuItem = menuItemComp.createObject(menu._contentColumn, {"text" : shuffleModel.get(i).text});
        }
    }

    function isInt(n) {
        return n !== "" && n % 1 === 0;
    }

    function isLetter(n) {
        return n.length === 1 && n.match(/[a-z]/i);
    }

    function isGrade(n) {
        for (var i = 0; i < lmDetails.count; i++) {
            if(lmDetails.get(i).grade === n){
                return true;
            }
        }
        return false;
    }

    function getIndexOfLetter(n)
    {    
        var alphabet = "abcdefghijklmnopqrstuvwxyz".split("");

        for (var i = 0; i < alphabet.length; i++) {
            if(alphabet[i] == n.toLowerCase()){
                return i;
            }
        }
    }

    function getIndexOfGrade(grade) {
        for (var i = 0; i < lmDetails.count; i++) {
            if(lmDetails.get(i).grade == grade){
                return i;
            }
        }
    }

    function getLetterFromIndex(letterIndex) {
        var alphabet = "abcdefghijklmnopqrstuvwxyz".split("");
        return alphabet[letterIndex];
    }

    function getGradeFromIndex(gradeIndex){
        return lmDetails.get(gradeIndex).grade;
    }

    function validate(){
        btnShuffle.text = ""
        btnShuffle.enabled = false
        if(!isInt(tbxMin.text) && shuffleScale === 0){
            tbxMin.label = "Fill in integer!";
            return;
        }
        if(!isInt(tbxMax.text) && shuffleScale === 0){
            tbxMax.label = "Fill in integer!";
            return;
        }
        if(parseInt(tbxMin.text) >= parseInt(tbxMax.text) && shuffleScale === 0){            
            tbxMin.label = "The max. value must be greater ...";
            tbxMax.label = "... than the min. value.";
            return;
        }
        if(tbxMin.text.length > 1 && shuffleScale === 1){
            tbxMin.label = "Fill in single letter!";
            return;
        }
        if(tbxMax.text.length > 1 && shuffleScale === 1){
            tbxMax.label = "Fill in single letter!";
            return;
        }
        if(!isLetter(tbxMin.text) && shuffleScale === 1){
            tbxMin.label = "Fill in letter!";
            return;
        }
        if(!isLetter(tbxMax.text) && shuffleScale === 1){
            tbxMax.label = "Fill in letter!";
            return;
        }
        if(getIndexOfLetter(tbxMin.text) >= getIndexOfLetter(tbxMax.text) && shuffleScale === 1){
            tbxMin.text = "";
            btnShuffle.enabled = false
        }
        if(!isGrade(tbxMin.text) && shuffleScale === 2){
            tbxMin.label = "Fill in grade!";
            return;
        }
        if(!isGrade(tbxMax.text) && shuffleScale === 2){
            tbxMax.label = "Fill in grade!";
            return;
        }
        if(getIndexOfGrade(tbxMin.text) >= getIndexOfGrade(tbxMax.text) && shuffleScale === 2){
            tbxMin.label = "The max. grade must be higher ...";
            tbxMax.label = "... than the min. grade.";
            return;
        }
        tbxMin.label = "Generate random " + shuffleModel.get(shuffleScale).text + " between " + tbxMin.text + " ...";
        tbxMax.label = "... and " + tbxMax.text + ". Press button!";
        btnShuffle.text = "Generate random " + shuffleModel.get(shuffleScale).text + " (" + tbxMin.text + "-" + tbxMax.text + ")";
        shuffleMin = tbxMin.text;
        shuffleMax = tbxMax.text;
        btnShuffle.enabled = true;
    }

    Column {
        id: column
        width: page.width
        spacing: Theme.paddingLarge
        PageHeader {
            title: qsTr("Generate random value.")
        }

        ComboBox {
            id: cbxShuffleScale
            width: parent.width
            label: "Shuffle Scale:"
            menu: ContextMenu{
                id: menu
            }
            onCurrentIndexChanged: {
                shuffleScale = currentIndex;
                if(shuffleScale < 2){
                    tbxMin.text = shuffleModel.get(currentIndex).tbxMin;
                    tbxMax.text = shuffleModel.get(currentIndex).tbxMax;
                    tbxMin.label = shuffleModel.get(currentIndex).lblMin;
                    tbxMax.label = shuffleModel.get(currentIndex).lblMax;
                    btnShuffle.text = "Generate random " + shuffleModel.get(currentIndex).text + " (" + tbxMin.text +"-" + tbxMax.text + ")";
                }
                if(shuffleScale === 2){
                    var z = lmDetails.get(lmDetails.count - 1).grade;
                    var y = lmDetails.get(lmDetails.count - 2).grade;
                    var b = lmDetails.get(1).grade;
                    var a = lmDetails.get(0).grade;

                    tbxMin.text = a;
                    tbxMin.label = "Fill in lowest " + shuffleModel.get(currentIndex).text + " ( " + a + ", " + b + ", ... )";
                    tbxMax.text = z;
                    tbxMax.label = "Fill in highest " + shuffleModel.get(currentIndex).text + " ( " + z + ", " + y + " ... )";
                    btnShuffle.text = shuffleModel.get(currentIndex).btnText + " ( " + a + " ... " + z + " )";
                }
            }
        }
        
        TextField {
            id: tbxMin
            width: parent.width
            text: shuffleMin
            label: "Generate random number between " + tbxMin.text + " ..."
            focusOnClick: true
            onClicked: { focus = true; text = ""}
            EnterKey.onClicked:{ parent.focus = true; }
            onTextChanged: {
                if(focus){
                    validate();
                }
            }
        }

        TextField {
            id: tbxMax
            width: parent.width
            text: shuffleMax
            label: "... and " + tbxMax.text + ".     Press button!"
            focusOnClick: true
            onClicked: { focus = true; text = ""}
            EnterKey.onClicked:{ parent.focus = true; }
            onTextChanged: {
                if(focus)
                {
                    validate();
                }
            }
        }

        Button {id:btnShuffle; x: 20; width: parent.width - 40; text: "Generate random number (" + tbxMin.text + "-" + tbxMax.text + ")";
            onClicked:
            {
                if(shuffleScale === 0){
                    btnShuffle.text = Math.floor(Math.random()*(parseInt(tbxMax.text) - parseInt(tbxMin.text) + 1) + parseInt(tbxMin.text));
                }
                if(shuffleScale === 1){
                    btnShuffle.text = (getLetterFromIndex(Math.floor(Math.random()*(getIndexOfLetter(tbxMax.text) - getIndexOfLetter(tbxMin.text) + 1) + getIndexOfLetter(tbxMin.text)))).toUpperCase();
                }
                if(shuffleScale === 2){
                    var iMax = getIndexOfGrade(tbxMax.text);                    
                    var iMin = getIndexOfGrade(tbxMin.text);
                    var i = Math.floor(Math.random()*(iMax - iMin + 1) + iMin);
                    btnShuffle.text = getGradeFromIndex(i);
                }
            }
        }
        Label{
            id: lblError;
            width: btnShuffle.width;
            anchors.left: btnShuffle.left;
            horizontalAlignment: TextInput.AlignHCenter;
            truncationMode: TruncationMode.Fade;
            maximumLineCount: 2;
            wrapMode: Text.WordWrap;
            text: "";
        }
    }
}
