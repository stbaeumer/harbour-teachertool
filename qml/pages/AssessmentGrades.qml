import QtQuick 2.0;
import Sailfish.Silica 1.0;

Page {
    id: page;
    allowedOrientations: Orientation.Portrait;

    function barWidth(freq){
        var maxFrequent = 0;
        for (var i = 0; i < lmDetails.count ; ++i) {
            maxFrequent = Math.max(lmDetails.get(i).frequency, maxFrequent);
        }
        var ret = ((page.width - 100) / maxFrequent) * freq + 40;
        return ret;
    }

    SilicaListView {
           id: listView;
           model: lmDetails;
           anchors.fill: parent;
           header: PageHeader {
               title: qsTr("Diagram");
            }
            delegate: ListItem {
                id: delegate;
                height: 50;
                /*
                menu: ContextMenu {
                    MenuItem {
                        text: "Count down";
                        onClicked: {
                            lmDetails.get(index).frequency -= 1;
                            number--;                            
                        }
                    }
                }
                */
                Rectangle {
                    id: delegateItem;
                    width: parent.width - 20; 
                    height: 40;
                    x: 10;
                    color: "transparent";
                    Rectangle{
                        id:rectleft;
                        width: 40;
                        height: parent.height;
                        color: "transparent";
                            Text {
                                id: itexItem;
                                color: "white";
                                text: grade;
                                font.bold: true;
                            }
                        }

                    Rectangle{
                        id:rectGrade;
                        anchors.left: rectleft.left;
                        height: 40;
                        width: 40;
                        color: "red";
                            Text {
                                id: it;
                                color: "white";
                                text: grade;
                                font.bold: true;
                            }
                        }

                     Rectangle{
                        id:rectMiddle;
                        anchors.left: rectleft.right;
                        height: 40;
                        width: 40;
                        color: "green";
                        PropertyAnimation { id: animation; target: rectMiddle;
       property: "width"; to: page.barWidth(frequency); duration: 1500 }
                        Component.onCompleted: {
                           animation.start();
                    }
               }
                Text{
                    id:textRight;
                    width: 30;
                    height: 40;
                    text: frequency;
                    color: "white";
                    font.bold: true;
                    anchors.right: rectMiddle.right;
               }
            }
        }
        VerticalScrollDecorator {}
    }
}
