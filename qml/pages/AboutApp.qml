import QtQuick 2.0
import Sailfish.Silica 1.0;

Page {
    id: page;
    allowedOrientations: Orientation.Portrait;

    Label {
        x: Theme.paddingLarge;
        horizontalAlignment: TextInput.AlignHCenter;
        verticalAlignment: TextInput.AlignVCenter;
        truncationMode: TruncationMode.Fade;
        width: parent.width;
        wrapMode: Text.WordWrap;

        text: qsTr("\n\n\n\n\n\n2018 - Stefan Bäumer\n\n\nIcons by Justin Ternet. Thank you!\n\nAny suggestions?\n\nThen send me a ping: \nbaeumer@posteo.de");
        //anchors.verticalCenter: parent.verticalCenter;
    }
}
