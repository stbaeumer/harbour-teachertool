import QtQuick 2.0;
import Sailfish.Silica 1.0;

Page {
    id: page;
    allowedOrientations: Orientation.Portrait;

    SilicaListView {
           id: listView;
           model: modelComparison;
           anchors.fill: parent;
           header: PageHeader {
               title: qsTr("Scale comparison");
            }
            delegate: BackgroundItem {
                id: deleate;
                height: 50;

                Row{
                    Text{
                        id:textLeft;
                        text: tLeft;
                        width: 180;
                        color: "white";
                        font { family: 'Courier'; pixelSize: 30; capitalization: Font.SmallCaps }
                    Text{
                        id:textMiddle;
                        anchors.left: textLeft.right;
                        text: "from " + tMiddle + "%";
                        width: 150;
                        color: "white";
                        font { family: 'Courier'; pixelSize: 30; capitalization: Font.SmallCaps }
                        }
                    Text{
                        id:textRight;
                        anchors.left: textMiddle.right;
                        text: tRight;
                        color: "white";
                        font { family: 'Courier'; pixelSize: 30; capitalization: Font.SmallCaps }
                    }
                }
            }
        }
    }
}






