import QtQuick 2.0
import Sailfish.Silica 1.0
import "../config.js" as DB;


Page {
    id: page

    function editLmDetailsModel(i){
        lmDetails.clear();
        console.log(lmm.count + " " + i);
        if(lmm.count > 0){
            var y = lmm.get(i).steps.split(",");
            var z = lmm.get(i).grades.split(",");
            var fromPercent = 0;

            for (var j = 0; j < z.length; j++){
                if(j > 0){
                    fromPercent = parseInt(lmDetails.get(j - 1).upToLessThanPercent);
                }
                lmDetails.append({grade: z[j], fromPercent: fromPercent, upToLessThanPercent: y[j], frequency: 0})
                console.log("!!Grade: " + z[j] + "; FromPercent: " + fromPercent + "; UpToLessThanPercent: " + y[j])
            }
        }
        return lmDetails
    }

    function createGradeString(j){
        var y = lmm.get(j).steps.split(",");
        var z = lmm.get(j).grades.split(",");
        var gradeString ="";

        for (var i = 0; i < z.length; i++){

            //gradeString += z[i] + "(<" + y[i] + "%),";
            gradeString += z[i] + ",";
        }        

        return gradeString;
    }

    function deleteScale(){
        for (var i = 0; i<lmm.count; ++i){
            if (lmm.get(i).selected){
                lmm.remove(i);
                i = 0;
            }
        }
    }

    function lmmNameIsUnique(name){
        for (var i = 0; i<lmm.count; ++i){
            if (lmm.get(i).name === name){
                return false;
            }
        }
        return true;
    }

    SilicaListView {
        anchors.fill: parent;
        header: Column {
            id: column
            width: parent.width
            height: header.height + mainColumn.height + Theme.paddingLarge

            PageHeader {
                id: header
                title: "Grading scales";
            }

            Column {
                id: mainColumn
                width: parent.width
                spacing: Theme.paddingLarge

                TextField{
                    id: tbxScale
                    label: "Name of new grading scale"
                    text: "My personal scale"
                    width: page.width;
                }

                Button {
                    id: btnAddScale
                    text: "Add scale"
                    anchors.horizontalCenter: parent.horizontalCenter;
                    width: parent.width - 60;
                    onClicked: {
                        var name = tbxScale.text;

                        while (!lmmNameIsUnique(name)) {
                            name = name + "_1";
                        }

                        lmm.append({name: name, steps: "100", grades: ":-)"});
                        DB.addScale(name,"100",":-)");
                        assessmentScaleIndex = lmm.count - 1;
                        DB.setAssessmentGradeIndex(assessmentScaleIndex);
                        editLmDetailsModel(assessmentScaleIndex);                        
                        pageStack.push(Qt.resolvedUrl("ScaleDetails.qml"));                        
                    }
                }                
            }
        }

        model: lmm
        delegate: ListItem {
            id: listEntry
            width: parent.width
            onClicked: {                
                if(assessmentScaleIndex != index){
                    assessmentScaleIndex = index;
                    DB.setAssessmentGradeIndex(assessmentScaleIndex);
                    editLmDetailsModel(assessmentScaleIndex);
                }
                pageStack.pop();
            }

            Label {
                id: lbl
                color: listEntry.highlighted ? Theme.highlightColor : Theme.primaryColor
                text: name
                x: Theme.horizontalPageMargin
                anchors.verticalCenter: parent.verticalCenter
            }

            menu: ContextMenu {
                MenuItem {
                    text: "Delete this scale"
                    onClicked: {
                        listEntry.remorseAction("Deleting", function() {
                            DB.removeScale(name)
                            console.log("model.index: " +model.index + "assessmentScaleIndex:" + assessmentScaleIndex)
                            if(model.index === assessmentScaleIndex){
                                assessmentScaleIndex = 0;
                            }
                            editLmDetailsModel(assessmentScaleIndex);                            
                            lmm.remove(model.index)                            
                        })
                    }
                }
                MenuItem {
                    text: "Edit this scale"
                    onClicked: {                        
                        assessmentScaleIndex = index;
                        DB.setAssessmentGradeIndex(assessmentScaleIndex);
                        console.log("Index " + index)
                        editLmDetailsModel(assessmentScaleIndex);
                        pageStack.push(Qt.resolvedUrl("ScaleDetails.qml"));
                        }
                    }
                }
            }

        VerticalScrollDecorator {}
    }
}
