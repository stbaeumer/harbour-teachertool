import QtQuick 2.0;
import Sailfish.Silica 1.0;

Page {
    id: page;
    allowedOrientations: Orientation.Portrait;

    Component {
    id: menuItemCompL
    MenuItem {}
    }

    Component {
    id: menuItemCompR
    MenuItem {}
    }

    Component.onCompleted: {
        if(lmm.count < 2){
            lblError.text = "Create a new scale to use comparison!";
            if(lmm.count === 0){
                lblError.text = "Create at least two scales to use comparison!";
            }
            btnCompare.enabled = false;
        }else{
            for (var i = 0; i < lmm.count ; i++){
                var newMenuItem = menuItemCompR.createObject(menuRight._contentColumn, {"text" : lmm.get(i).name});
            }
            for (i = 0; i < lmm.count ; i++){
                newMenuItem = menuItemCompL.createObject(menuLeft._contentColumn, {"text" : lmm.get(i).name});
            }

            cbxScaleLeft.currentIndex = assessmentScaleIndex;
            indexScaleLeft = assessmentScaleIndex;

            if(assessmentScaleIndex === 0){
                cbxScaleRight.currentIndex = 1;
                indexScaleRight = 1;
            }else{
                cbxScaleRight.currentIndex = 0;
                indexScaleRight = 0;
            }

            lblError.text = "";
            btnCompare.enabled = true;
        }
    }

    function compare() {        
        if(indexScaleRight != indexScaleLeft) {
            lblError.text = "";
            btnCompare.enabled = true;
        }
        else {
            lblError.text = "You need to select different scales!";
            btnCompare.enabled = false;
        }
    }

    function createModelL(i){
        var y = lmm.get(i).steps.split(",");
        var z = lmm.get(i).grades.split(",");
        var fromPercent = 0;

        for (var j = 0; j < z.length; j++){
            if(j > 0){
                fromPercent = parseInt(ml.get(j - 1).upToLessThanPercent);
            }
            ml.append({grade: z[j], fromPercent: fromPercent, upToLessThanPercent: y[j], frequency: 0})            
        }     
    }

    function createModelR(i){
        var y = lmm.get(i).steps.split(",");
        var z = lmm.get(i).grades.split(",");
        var fromPercent = 0;

        for (var j = 0; j < z.length; j++){
            if(j > 0){
                fromPercent = parseInt(mr.get(j - 1).upToLessThanPercent);
            }
            mr.append({grade: z[j], fromPercent: fromPercent, upToLessThanPercent: y[j], frequency: 0})            
        }
    }
    
    ListModel{
    id: ml
    }

    ListModel{
    id: mr
    }

    function go() {
        modelComparison.clear();
        ml.clear();
        mr.clear();
        createModelR(indexScaleRight);
        createModelL(indexScaleLeft);

        for (var i = 0; i < 100; i++) {
            var left = "";
            var right = "";
            var middle = i;

            for (var j = 0; j < ml.count; j++) {
                if(ml.get(j).fromPercent === i){
                    left = ml.get(j).grade;
                }
            }

            for (var j = 0; j < mr.count; j++) {
                if(mr.get(j).fromPercent === i){
                    right = mr.get(j).grade;
                }
            }

            if(left != "" || right != "") {
                modelComparison.append({"tLeft" : "     " + left, "tMiddle" : i + "", "tRight" : right});
            }
        }
        pageStack.push(Qt.resolvedUrl("ScaleComparison.qml"));
    }

    Column {
        id: column;
        width: page.width;
        spacing: Theme.paddingLarge;
        PageHeader {
            title: qsTr("Select 2 different scales");
        }
        ComboBox {
            id: cbxScaleLeft;
            width: parent.width;
            label: "Scale left:";            
            menu: ContextMenu{
                id: menuLeft

            }
            onCurrentIndexChanged: {
                indexScaleLeft = currentIndex;
                compare();
            }
        }
        ComboBox {
            id: cbxScaleRight;
            width: parent.width;
            label: "Scale right:";            
            menu: ContextMenu{                
                id: menuRight
            }
            onCurrentIndexChanged: {
                indexScaleRight = currentIndex;
                compare();
            }
        }
        Button {
            id:btnCompare; 
            x: 20; 
            width: parent.width - 40;
            text: "Compare scales";
            onClicked: {
                go();
            }
        }
        Label{
            id: lblError;
            width: btnCompare.width;
            anchors.left: btnCompare.left;
            horizontalAlignment: TextInput.AlignHCenter;
            truncationMode: TruncationMode.Fade;
            maximumLineCount: 2;
            wrapMode: Text.WordWrap;
            text: "";
        }
    }
}
