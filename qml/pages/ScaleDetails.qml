import QtQuick 2.0;
import Sailfish.Silica 1.0;
import "../config.js" as DB;

Page {
    id: page;
    allowedOrientations: Orientation.Portrait;

    function getValue(fromPercent, upToLessThanPercent) {
        var begin = "";
        var end ="";
        if(fromPercent === 0) {
            begin = "  :   " + fromPercent;
        }
        else {
            begin = "  :  " + fromPercent;
        }
        if(upToLessThanPercent < 100) {
            end = "% <= x < " + upToLessThanPercent + "%";
        }
        else {
            end = "% <= x <=" + Math.min(upToLessThanPercent,100) + "%";
        }
        return begin + end;
    }

    function deleteGrade(){

    }

    function updateLmm(){

        if(lmDetails.count > 0)
        {
            var grades = lmDetails.get(0).grade;
            var steps = lmDetails.get(0).upToLessThanPercent;

            for (var i = 1; i < lmDetails.count; i++) {

                grades = grades + "," + lmDetails.get(i).grade;
                steps = steps + "," + lmDetails.get(i).upToLessThanPercent;
            }

            lmm.get(assessmentScaleIndex).grades = grades;
            lmm.get(assessmentScaleIndex).steps = steps;

            DB.updateModelDetails(lmm.get(assessmentScaleIndex).name,lmm.get(assessmentScaleIndex).steps,lmm.get(assessmentScaleIndex).grades);
        }else{
            lmm.get(assessmentScaleIndex).grades = null;
            lmm.get(assessmentScaleIndex).steps = null;
            DB.updateModelDetails(lmm.get(assessmentScaleIndex).name,"","");
        }
    }

    function getGrade(percent){
        for (var i = 0; i < lmDetails.count; i++) {
            if(parseInt(percent) === parseInt(lmDetails.get(i).upToLessThanPercent)){
                return i;
            }
        }
    }

    function sortLmDetails(){

        var items = new Array();
        var absMin = 100;

        for (var i = 0; i < lmDetails.count; i++) {
            if(parseInt(lmDetails.get(i).upToLessThanPercent) < absMin){
                absMin = lmDetails.get(i).upToLessThanPercent;        
            }
        }

        for (var z = 0; z < lmDetails.count; z++) {
            console.log("lmDetails " + z + ". " + lmDetails.get(z).grade + "---" + lmDetails.get(z).upToLessThanPercent)
        }

        items.push(absMin)

        l.append({grade: lmDetails.get(getGrade(absMin)).grade, upToLessThanPercent: absMin + ""})

        for (var z = 0; z < lmDetails.count; z++) {

            console.log("lmDetails.count" + lmDetails.count)

            if(absMin != lmDetails.get(z).upToLessThanPercent){

                var min = 100;

                for (var i = 0; i < lmDetails.count; i++) {
                    if(lmDetails.get(i).upToLessThanPercent <= min) {
                        if(lmDetails.get(i).upToLessThanPercent > items[items.length - 1]) {
                            min = lmDetails.get(i).upToLessThanPercent;
                        }
                    }
                }
                items.push(min)
                console.log("Min: " + min)
                l.append({grade: lmDetails.get(getGrade(min)).grade, upToLessThanPercent: min + ""})
            }
        }

        for (var z = 0; z < l.count; z++) {
            console.log("l " + z + ". " + l.get(z).grade + "---" + l.get(z).upToLessThanPercent)
        }

        lmDetails.clear();

        for (var i = 0; i < l.count; i++) {

                lmDetails.append({grade: l.get(i).grade, upToLessThanPercent: l.get(i).upToLessThanPercent});
        }

        l.clear();

        // Append to model

        var steps = "";
        var grades = "";
        var name = lmm.get(assessmentScaleIndex).name;
        for (var z = 0; z < lmDetails.count; z++) {

            steps += lmDetails.get(z).upToLessThanPercent + ",";
            grades += lmDetails.get(z).grade + ",";

            console.log(z + ". " + lmDetails.get(z).grade + "---" + lmDetails.get(z).upToLessThanPercent)
        }

        lmm.remove(assessmentScaleIndex)

        lmm.append({name: name, steps: steps.substring(0, steps.length - 1), grades: grades.substring(0, grades.length - 1)});

        for (var z = 0; z < lmm.count; z++) {
            console.log(lmm.get(assessmentScaleIndex).name + " ... " + lmm.get(assessmentScaleIndex).steps + " ... " + lmm.get(assessmentScaleIndex).grades);
        }
    }

    function getDescription(i){
        if(i >= 0 && lmDetails.count > 0)
        {
            console.log("................")
            var description = "";

            var lowerBorder = 0
            if(i > 0) {
                lowerBorder = lmDetails.get(i - 1).upToLessThanPercent
            }

            var lessThan = "<"

            if((i + 1) === lmDetails.count)
            {
                lessThan = "\u2264";
            }

            console.log("index: " + i + " lmDetails " + lmDetails)
            console.log("index: " + i + " lmDetails.get(0) " + lmDetails.get(0))
            console.log("index: " + i + " lmDetails.get(i).upToLessThanPercent " + lmDetails.get(i).upToLessThanPercent)

            description = lowerBorder + " \u2264 x " + lessThan + " " + lmDetails.get(i).upToLessThanPercent

            return lmDetails.get(i).grade +  "  ( " + description + " )";
        }
        return "";
    }

    function gradeUnique(grade){
        for (var z = 0; z < lmDetails.count; z++) {
            if(grade === lmDetails.get(z).grade){
                return false;
            }
        }
        return true;
    }

    function upToLessThanPercentUnique(percent){
        for (var z = 0; z < lmDetails.count; z++) {
            if(percent === parseInt(lmDetails.get(z).upToLessThanPercent)){
                return false;
            }
        }
        return true;
    }

    function warning100(){
        for (var z = 0; z < lmDetails.count; z++) {
            if(100 === parseInt(lmDetails.get(z).upToLessThanPercent)){
                return "";
            }
        }
        return "Don't forget to define exact one grade up to 100 %.";
    }

    SilicaListView {

        anchors.fill: parent
        header: Column {
            width: parent.width
            height: header.height + mainColumn.height + Theme.paddingLarge

            PageHeader {
                id: header
                title: "Details of " + lmm.get(assessmentScaleIndex).name
            }

            Column {
                id: mainColumn
                width: parent.width
                spacing: Theme.paddingLarge

                Row{
                    TextField{
                        id: tbxGrade
                        label: "grade"
                        text: lmDetails.get(0).grade
                        width: page.width / 3;
                        anchors.bottom: slider.bottom
                    }
                    Slider {
                        id: slider
                        label: "up to less %"
                        width: parent.width / 3 * 2
                        minimumValue: 1; maximumValue: 100; stepSize: 1
                        valueText: Math.max(1, value)
                    }
                }

                Button {
                    id: btnAdd
                    text: "Add grade to scale"
                    anchors.horizontalCenter: parent.horizontalCenter;
                    width: parent.width - 60;
                    onClicked: {

                        var str = tbxGrade.text;
                        if(str.match(',')){
                            lblWarning.text = "Using ',' in grade is not allowed.";
                            return;
                        }

                        if(!gradeUnique(tbxGrade.text)){
                            for (var z = 0; z < lmDetails.count; z++) {
                                if(tbxGrade.text === lmDetails.get(z).grade){                                    
                                    lmDetails.remove(z);
                                }
                            }
                        }
                        if(!upToLessThanPercentUnique(slider.value)){
                            for (var z = 0; z < lmDetails.count; z++) {
                                if(slider.value === parseInt(lmDetails.get(z).upToLessThanPercent)){
                                    console.log("Remove percent: " + lmDetails.get(z).upToLessThanPercent)
                                    lmDetails.remove(z);
                                }
                            }
                        }
                        lmDetails.append({grade: tbxGrade.text, upToLessThanPercent: (slider.value + "")});
                        updateLmm("");
                        console.log("grade: "+  tbxGrade.text + "upToLessThanPercent:" + (slider.value + ""))
                        sortLmDetails();

                        lblWarning.text = warning100();
                    }
                }
                Label {
                    id: lblWarning;                                        
                    width: btnAdd.width
                    anchors.left: btnAdd.left;
                    horizontalAlignment: TextInput.AlignHCenter;
                    truncationMode: TruncationMode.Fade;
                    maximumLineCount: 2;
                    wrapMode: Text.WordWrap;
                    color: Theme.primaryColor;
                }
            }
        }

        footer: Column {
        }

        model: lmDetails
        delegate: ListItem {
            id: listEntry
            width: parent.width

            Label {
                id: lbl
                color: listEntry.highlighted ? Theme.highlightColor : Theme.primaryColor
                text: {                        
                    getDescription(index);
                }
                x: Theme.horizontalPageMargin
                anchors.verticalCenter: parent.verticalCenter
            }

            menu: ContextMenu {
                MenuItem {
                    text: "Delete grade"
                    onClicked: {
                        listEntry.remorseAction("Deleting", function() {
                            console.log(lmDetails.count)
                            lmDetails.remove(model.index);
                            console.log(lmDetails.count)
                            updateLmm();
                        })
                    }
                }
            }
        }
        VerticalScrollDecorator {}
    }
}






