//config.js

.import QtQuick.LocalStorage 2.0 as LS

function getDatabase() {
    console.log("Get database ...");
    return LS.LocalStorage.openDatabaseSync("harbour-teachertool2", "1.0", "StorageDatabase", 100000);
}

function initialize() {
    var db = getDatabase();
    console.log("Create tables ...");
    db.transaction(
                function(tx) {                    
                    tx.executeSql('CREATE TABLE IF NOT EXISTS AssessmentGradeIndex(id INT PRIMARY KEY, ind INT)');
                });
    db.transaction(
                function(tx) {
                    tx.executeSql('CREATE TABLE IF NOT EXISTS FirstRunEver(id INT PRIMARY KEY, runBefore INT)');
                });
    db.transaction(
                function(tx) {
                    tx.executeSql('INSERT OR IGNORE INTO FirstRunEver(id,runBefore) VALUES(1,0);');
                });
    db.transaction(
                function(tx) {
                    tx.executeSql('INSERT OR IGNORE INTO AssessmentGradeIndex(id,ind) VALUES(1,0);');
                });
    db.transaction(
                function(tx) {
                    tx.executeSql('CREATE TABLE IF NOT EXISTS Scales(name CHAR PRIMARY KEY, steps CHAR, grades CHAR)');
                });

    var scalesAlreadyInitialized = 0;

    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('SELECT runBefore FROM FirstRunEver where id = 1');
                    scalesAlreadyInitialized = rs.rows[0].runBefore;
                    console.log("scalesAlreadyInitialized " + rs.rows[0].runBefore)
                });

    // Initially insert predefined scales

    if(scalesAlreadyInitialized === 0){

        console.log("Scales not yet initially inialized.");

        var names = [
                    "Germany",
                    "Germany IHK",
                    "Germany Abi Punkte",
                    "Belgium",
                    "Austria",
                    "China",
                    "Etcs",
                    "Israel",
                    "Japan",
                    "Russia",
                    "Spain",
                    "Turkey",
                    "USAHighSchool",
                    "UK Advanced"
                ];
        var steps = [
                    "20,27,34,41,46,51,56,61,66,71,76,81,86,91,96,100",
                    "30,50,67,81,92,100",
                    "20,27,34,41,46,51,56,61,66,71,76,81,86,91,96,100",
                    "60,68,78,86,100",
                    "50,64,80,90,100",
                    "60,64,68,72,75,78,82,85,90,100",
                    "50,55,60,70,90,95,100",
                    "45,55,65,75,85,95,100",
                    "60,70,80,90,100",
                    "77,85,93,100",
                    "50,55,70,90,95,100",
                    "50,60,70,85,100",
                    "60,70,80,90,100",
                    "50,60,70,80,90,100"
                ];
        var grades = [
                    "6,5-,5,5+,4-,4,4+,3-,3,3+,2-,2,2+,1-,1,1+",
                    "6,5,4,3,2,1",
                    "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15",
                    "F,S,D,GD,PGD",
                    "5,4,3,2,1",
                    "F,D,C-,C,C+,B-,B,B+,A-,A",
                    "FX-F,E,D,C,B,A,A*",
                    "<4,5,6,7,8,9,10",
                    "E/F,C,B,A,AA",
                    "2,3,4,5",
                    "5,5.5,6,7,9,10",
                    "1,2,3,4,5",
                    "F,D,C,B,A",
                    "E,D,C,B,A,A*"
                ];

        for(var i = 0; i < names.length; i++){
            console.log("Read predefined scales from DB: " + names[i]);
            addScale(names[i],steps[i],grades[i])
        }

        db = getDatabase();
        db.transaction(function(tx) {
            var rs = tx.executeSql("UPDATE FirstRunEver SET runBefore = 1 WHERE id=1;");
        }
        );
    }
}

function getModel(){
    var db = getDatabase();
    db.transaction(function(tx) {
        var rs = tx.executeSql('SELECT * FROM Scales;');
        for (var i = 0; i < rs.rows.length; i++) {
            lmm.append({name: rs.rows.item(i).name, steps: rs.rows.item(i).steps, grades: rs.rows.item(i).grades})      
            console.log("lmm.append: name:" + rs.rows.item(i).name + " steps:" + rs.rows.item(i).steps +" grades:" + rs.rows.item(i).grades)
        }
    })    
}

function updateModelDetails(name,steps,grades){
    var db = getDatabase();
    db.transaction(function(tx) {        
        var rs = tx.executeSql("UPDATE Scales SET steps = '" + steps + "', grades = '" + grades + "' WHERE name='" + name + "';");
    }
    );
}

function addScale(name,steps,grades){
    var db = getDatabase();
    db.transaction(
        function(tx) {
            tx.executeSql('INSERT OR IGNORE INTO Scales(name,steps,grades) VALUES(?,?,?);', [name,steps,grades]);
        });
}

function setAssessmentGradeIndex(i){
    var db = getDatabase();
    db.transaction(function(tx) {
        try {
            var rs = tx.executeSql("UPDATE AssessmentGradeIndex SET ind = '" + i + "' WHERE id=1");
            console.log("Update" + rs);
        }
        catch(err) {
            console.log("Fehler " + err.message);
        }
    }
    );    
}

function getAssessmentGradeIndex(){
    var db = getDatabase();
    db.transaction(function(tx) {
        var rs = tx.executeSql('SELECT DISTINCT ind FROM AssessmentGradeIndex;');
        assessmentScaleIndex = rs.rows.item(0).ind;
    })

    assessmentScaleIndex = Math.min(assessmentScaleIndex, Math.max(lmm.count - 1, 0));

    console.log("assessmentScaleIndex: " + assessmentScaleIndex);
}

function removeScale(name) {
    var db = getDatabase();    
    db.transaction(function(tx) {
        var rs = tx.executeSql('DELETE FROM scales WHERE name=?;' , [name]);
    })
}
