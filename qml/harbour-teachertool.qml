/*
  Copyright (C) 2017 Stefan Bäumer
  Contact: baeumer@posteo.de
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "pages"

ApplicationWindow
{
    initialPage: Component { FirstPage { } }
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: Orientation.All
    _defaultPageOrientations: Orientation.All

    property int assessmentScaleIndex: 0
    property int maxPoints: 100
    property int reachedPoints: 0
    property int missedPoints: maxPoints - reachedPoints
    property int mode: 0
    property int gradeIndex: -1
    property var modelRepeater
    property int sumOfAllIndexes: 0
    property int sumOfAllPercents: 0.0
    property var number: 0
    property var avgPercent: 0
    property var avgGrade: 0
    property var avgIndex: 0
    property var indexScaleLeft: 0
    property var indexScaleRight: 0

    // Shuffle

    property var shuffleMin : 1
    property var shuffleMax : 15
    property int shuffleScale : 0

    property var shuffleModel: modelShuffleScale

    ListModel{
        id: modelShuffleScale            
    }

    //property var models: [modelGermany, modelGermanyIhk, modelGermanyAbiPunkte, modelAustria, modelBelgium, modelChina, modelEtcs, modelIsrael, modelJapan, modelRussiaSchool, modelSpainSchool, modelTurkeySchool, modelUSAHighSchool, modelUkAdvanced, modelComparison]

    property ListModel lmm : ListModel {}

    property ListModel lmDetails: ListModel {}

    property var l : ll

    ListModel{
    id: ll
    }

    ListModel{
    id: entries
    }

    ListModel{
        id: modelComparison
    }
}
